//
// Created by Niyaz K on 27/09/20.
//

#include "catch.hpp"
#include "../lib/local_time.h"

using namespace Catch::Matchers;

TEST_CASE("local_time construction with hour argument") {

  SECTION("Given local_time constructed with invalid hour bigger than max allowed value of 23 should throw out_of_range") {
    REQUIRE_THROWS_AS(local_time::of(24), std::out_of_range);
    REQUIRE_THROWS_WITH(local_time::of(24), Contains("hour must be between (0, 23) inclusive, actual : 24"));
  }

  SECTION("Given local_time constructed with invalid hour less than min allowed value of 0 should throw out_of_range") {
    REQUIRE_THROWS_AS(local_time::of(-1), std::out_of_range);
    REQUIRE_THROWS_WITH(local_time::of(-1), Contains("hour must be between (0, 23) inclusive, actual : -1"));
  }

  SECTION("Given local_time constructed with valid hour(10) should create and return object") {
    auto time = local_time::of(10);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 0);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }

  SECTION("Given local_time constructed with valid hour(0) should create and return object") {
    auto time = local_time::of(0);
    REQUIRE(time.hour() == 0);
    REQUIRE(time.minute() == 0);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }

  SECTION("Given local_time constructed with valid hour(23) should create and return object") {
    auto time = local_time::of(23);
    REQUIRE(time.hour() == 23);
    REQUIRE(time.minute() == 0);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }
}

TEST_CASE("local_time construction with hour and minute argument") {

  SECTION("Given local_time constructed with invalid minute bigger than max allowed value of 59 should throw out_of_range") {
    REQUIRE_THROWS_AS(local_time::of(10, 60), std::out_of_range);
    REQUIRE_THROWS_WITH(local_time::of(10, 60), Contains("minute must be between (0, 59) inclusive, actual : 60"));
  }

  SECTION("Given local_time constructed with invalid hour less than min allowed value of 0 should throw invalid_argument") {
    REQUIRE_THROWS_AS(local_time::of(10, -1), std::out_of_range);
    REQUIRE_THROWS_WITH(local_time::of(10, -1), Contains("minute must be between (0, 59) inclusive, actual : -1"));
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(59) should create and return object") {
    auto time = local_time::of(10, 59);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 59);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(0) should create and return object") {
    auto time = local_time::of(10, 0);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 0);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(30) should create and return object") {
    auto time = local_time::of(10, 30);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 30);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }
}

TEST_CASE("local_time construction with hour and minute and second argument") {

  SECTION("Given local_time constructed with invalid second bigger than max allowed value of 59 should throw out_of_range") {
    REQUIRE_THROWS_AS(local_time::of(10, 40, 60), std::out_of_range);
    REQUIRE_THROWS_WITH(local_time::of(10, 40, 60), Contains("second must be between (0, 59) inclusive, actual : 60"));
  }

  SECTION("Given local_time constructed with invalid second less than min allowed value of 0 should throw invalid_argument") {
    REQUIRE_THROWS_AS(local_time::of(10, 40, -1), std::out_of_range);
    REQUIRE_THROWS_WITH(local_time::of(10, 40, -1), Contains("second must be between (0, 59) inclusive, actual : -1"));
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(30) and second(59) should create and return object") {
    auto time = local_time::of(10, 30, 59);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 30);
    REQUIRE(time.second() == 59);
    REQUIRE(time.nano_second() == 0);
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(40) second(0)should create and return object") {
    auto time = local_time::of(10, 40, 0);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 40);
    REQUIRE(time.second() == 0);
    REQUIRE(time.nano_second() == 0);
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(30), second(45) should create and return object") {
    auto time = local_time::of(10, 30, 45);
    REQUIRE(time.hour() == 10);
    REQUIRE(time.minute() == 30);
    REQUIRE(time.second() == 45);
    REQUIRE(time.nano_second() == 0);
  }
}

TEST_CASE("== operator") {

  SECTION("Given both local_time are equivalent should return true") {
    REQUIRE(local_time::of(10) == local_time::of(10));
    REQUIRE(local_time::of(10, 30) == local_time::of(10, 30));
    REQUIRE(local_time::of(10, 30, 59) == local_time::of(10, 30, 59));
    REQUIRE(local_time::of(10, 30, 59, 123) == local_time::of(10, 30, 59, 123));
  }

  SECTION("Given both local_time are not equivalent should return false") {
    REQUIRE((local_time::of(10) == local_time::of(11)) == false);
    REQUIRE((local_time::of(10, 30) == local_time::of(10, 31)) == false);
    REQUIRE((local_time::of(10, 30, 59) == local_time::of(10, 30, 58)) == false);
    REQUIRE((local_time::of(10, 30, 59, 123) == local_time::of(10, 30, 59, 121)) == false);
  }

}

TEST_CASE("!= operator") {
  SECTION("Given both local_time are not equivalent should return true") {
    REQUIRE(local_time::of(10) != local_time::of(11));
    REQUIRE(local_time::of(10, 30) != local_time::of(10, 31));
    REQUIRE(local_time::of(10, 30, 59) != local_time::of(10, 30, 58));
    REQUIRE(local_time::of(10, 30, 59, 123) != local_time::of(10, 30, 59, 124));
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(30) should create and return object") {
    REQUIRE(local_time::of(10) < local_time::of(11));
    REQUIRE(local_time::of(10, 30) < local_time::of(10, 31));
    REQUIRE(local_time::of(10, 30, 57) < local_time::of(10, 30, 58));
    REQUIRE(local_time::of(10, 30, 59, 12) < local_time::of(10, 30, 59, 124));
  }

  SECTION("Given local_time constructed with valid hour(10) and minute(30) should create and return object") {
    REQUIRE(local_time::of(11) > local_time::of(10));
    REQUIRE(local_time::of(10, 32) > local_time::of(10, 31));
    REQUIRE(local_time::of(10, 30, 59) > local_time::of(10, 30, 58));
    REQUIRE(local_time::of(10, 30, 59, 124) > local_time::of(10, 30, 59, 123));

    REQUIRE(local_time::of(10, 30, 59, 124).to_string() == "10:30:59:124");

    REQUIRE(local_time::of(10, 30, 59, 124).trunc_nano_second() == local_time::of(10, 30, 59, 0));
    REQUIRE(local_time::of(10, 30, 59, 124).trunc_second() == local_time::of(10, 30, 0, 124));
    REQUIRE(local_time::of(10, 30, 59, 124).trunc_minute() == local_time::of(10, 0, 59, 124));
    REQUIRE(local_time::of(10, 30, 59, 124).trunc_hour() == local_time::of(0, 30, 59, 124));

    auto x = local_time::of(22, 1, 40, 485635000);
    REQUIRE(x.plus_minute(123456789).to_nano_seconds() == 69040485635000L);

    auto y = local_time::of(22, 11, 30, 226036000);
    REQUIRE(y.plus_hour(1234123).to_nano_seconds() == 61890226036000L);

    auto z = local_time::of(22, 11, 30, 226036000);
    REQUIRE(z.plus_hour(-1234123).to_nano_seconds() == 11490226036000L);

    auto c = local_time::of(22, 25, 40, 391866000);
    REQUIRE(c.plus_minute(134123).to_nano_seconds() == 6520391866000L);

    REQUIRE(!local_time::now().to_string().empty());
  }

}

//SECTION( "Given local_time constructed with invalid minute bigger than max allowed value of 59 should throw invalid_argument" ) {
//REQUIRE_THROWS_AS(LocalTime::of(10, 60), std::invalid_argument);
//}
//
//SECTION( "Given local_time constructed with invalid minute less than min allowed value of 0 should throw invalid_argument" ) {
//REQUIRE_THROWS_AS(LocalTime::of(10, -1), std::invalid_argument);
//}
//
//SECTION( "Given local_time constructed with valid hour and minute should create and return object" ) {
//auto time = LocalTime::of(10, 20);
//REQUIRE(time.get_hour() == 10);
//REQUIRE(time.get_minute() == 20);
//REQUIRE(time.get_second() == 0);
//REQUIRE(time.get_nano_second() == 0);
//}
//
//SECTION( "Given local_time constructed with invalid second bigger than max allowed value of 59 should throw invalid_argument" ) {
//REQUIRE_THROWS_AS(LocalTime::of(10, 30, 61), std::invalid_argument);
//}
//
//SECTION( "Given local_time constructed with invalid second less than min allowed value of 0 should throw invalid_argument" ) {
//REQUIRE_THROWS_AS(LocalTime::of(10, 30, -1), std::invalid_argument);
//}
//
//SECTION( "Given local_time constructed with valid hour and minute and second, should create and return object" ) {
//auto time = LocalTime::of(10, 20, 45);
//REQUIRE(time.get_hour() == 10);
//REQUIRE(time.get_minute() == 20);
//REQUIRE(time.get_second() == 45);
//REQUIRE(time.get_nano_second() == 0);
//}
//
//SECTION( "Given local_time constructed with invalid nano_second bigger than max allowed value of 999999999 should throw invalid_argument" ) {
//REQUIRE_THROWS_AS(LocalTime::of(10, 30, 58, 1000000000), std::invalid_argument);
//}
//
//SECTION( "Given local_time constructed with invalid nano_second less than min allowed value of 0 should throw invalid_argument" ) {
//REQUIRE_THROWS_AS(LocalTime::of(10, 30, 32, -1), std::invalid_argument);
//}
//
//SECTION( "Given local_time constructed with valid hour and minute and second and nano_second, should create and return object" ) {
//auto time = LocalTime::of(10, 20, 45, 999872315);
//REQUIRE(time.get_hour() == 10);
//REQUIRE(time.get_minute() == 20);
//REQUIRE(time.get_second() == 45);
//REQUIRE(time.get_nano_second() == 999872315);
//}
//
//}
//
//TEST_CASE("equality operator") {
//
//SECTION( "== operator" ) {
//
//SECTION("Given two local_time instances have same time values should return true when comparing") {
//auto previous_time = LocalTime::of(10, 20, 45, 999872315);
//auto current_time = LocalTime::of(10, 20, 45, 999872315);
//REQUIRE(previous_time == current_time);
//}
//
//SECTION("Given two local_time instances have different time values should return true when comparing") {
//auto previous_time = LocalTime::of(10, 20, 45, 999872315);
//auto current_time = LocalTime::of(10, 20, 45, 199872315);
//REQUIRE_FALSE(previous_time == current_time);
//}
//
//}
//
//SECTION( "is_equal" ) {
//
//SECTION("Given two local_time instances have same time values should return true when comparing") {
//auto previous_time = LocalTime::of(10, 20, 45, 999872315);
//auto current_time = LocalTime::of(10, 20, 45, 999872315);
//REQUIRE(previous_time.is_equal(current_time) );
//}
//
//SECTION("Given two local_time instances have different time values should return true when comparing") {
//auto previous_time = LocalTime::of(10, 20, 45, 999872315);
//auto current_time = LocalTime::of(10, 20, 45, 199872315);
//REQUIRE_FALSE(previous_time.is_equal(current_time));
//}
//
//}
//
//}
//
//TEST_CASE("is_before method") {
//
//SECTION( "is_before given current time is after specified time should return false" ) {
//auto time_a = LocalTime::of(11, 20, 45, 999872315);
//auto time_b = LocalTime::of(10, 20, 45, 999872315);
//REQUIRE_FALSE(time_a.is_before(time_b));
//
//auto time_c = LocalTime::of(10, 21, 45, 999872315);
//REQUIRE_FALSE(time_c.is_before(time_b));
//
//auto time_d = LocalTime::of(10, 20, 46, 999872315);
//REQUIRE_FALSE(time_d.is_before(time_b));
//
//auto time_e = LocalTime::of(10, 20, 45, 999872316);
//REQUIRE_FALSE(time_e.is_before(time_b));
//
//}
//
//SECTION( "is_before given current time is before specified time should return true" ) {
//auto time_a = LocalTime::of(10, 20, 45, 999872315);
//auto time_b = LocalTime::of(11, 20, 45, 999872315);
//REQUIRE(time_a.is_before(time_b) == true);
//
//auto time_c = LocalTime::of(10, 21, 45, 999872315);
//REQUIRE(time_c.is_before(time_b) == true);
//
//auto time_d = LocalTime::of(10, 20, 46, 999872315);
//REQUIRE(time_d.is_before(time_b) == true);
//
//auto time_e = LocalTime::of(10, 20, 45, 999872316);
//REQUIRE(time_e.is_before(time_b) == true);
//}
//}
//
//TEST_CASE("is_after method") {
//
//SECTION( "is_after given current time is after specified time should return false" ) {
//auto time_a = LocalTime::of(11, 20, 45, 999872315);
//auto time_b = LocalTime::of(10, 20, 45, 999872315);
//REQUIRE(time_a.is_after(time_b) == true);
//
//auto time_c = LocalTime::of(10, 21, 45, 999872315);
//REQUIRE(time_c.is_after(time_b) == true);
//
//auto time_d = LocalTime::of(10, 20, 46, 999872315);
//REQUIRE(time_d.is_after(time_b) == true);
//
//auto time_e = LocalTime::of(10, 20, 45, 999872316);
//REQUIRE(time_e.is_after(time_b) == true);
//
//}
//
//SECTION( "is_after given current time is before specified time should return true" ) {
//auto time_a = LocalTime::of(10, 20, 45, 999872315);
//auto time_b = LocalTime::of(11, 20, 45, 999872315);
//REQUIRE_FALSE(time_a.is_after(time_b));
//
//auto time_c = LocalTime::of(10, 21, 45, 999872315);
//REQUIRE_FALSE(time_c.is_after(time_b));
//
//auto time_d = LocalTime::of(10, 20, 46, 999872315);
//REQUIRE_FALSE(time_d.is_after(time_b));
//
//auto time_e = LocalTime::of(10, 20, 45, 999872316);
//REQUIRE_FALSE(time_e.is_after(time_b));
//}
//}


