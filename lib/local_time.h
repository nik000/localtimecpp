//
// Created by Niyaz K on 27/09/20.
//

#ifndef LOCAL_TIME_V1_LOCAL_TIME_H
#define LOCAL_TIME_V1_LOCAL_TIME_H

#include <cstdint>
#include <string>
#include <chrono>

class local_time {

private:
    const int64_t hour_;
    const int64_t minute_;
    const int64_t second_;
    const int64_t nano_second_;
    const std::string format_;

    local_time(
            int64_t hour,
            int64_t minute,
            int64_t second,
            int64_t nanosecond);

    static local_time local_time_with_wrap(
            int64_t hour,
            int64_t minute,
            int64_t second,
            int64_t nanosecond);

public:
    [[nodiscard]] static local_time of(int64_t hour);
    [[nodiscard]] static local_time of(int64_t hour, int64_t minute);
    [[nodiscard]] static local_time of(int64_t hour, int64_t minute, int64_t second);
    [[nodiscard]] static local_time of(int64_t hour, int64_t minute, int64_t second, int64_t nano_second);
    [[nodiscard]] static local_time now();

    [[nodiscard]] local_time plus_hour(int64_t hour) const;
    [[nodiscard]] local_time plus_minute(int64_t minute) const;
    [[nodiscard]] local_time plus_second(int64_t second) const;
    [[nodiscard]] local_time plus_nano_second(int64_t nano_second) const;

    [[nodiscard]] local_time minus_hour(int64_t hour) const;
    [[nodiscard]] local_time minus_minute(int64_t minute) const;
    [[nodiscard]] local_time minus_second(int64_t second) const;
    [[nodiscard]] local_time minus_nano_second(int64_t nano_second) const;

    constexpr const std::string& to_string() const;

    [[nodiscard]] constexpr int64_t hour() const;
    [[nodiscard]] constexpr int64_t minute() const;
    [[nodiscard]] constexpr int64_t second() const;
    [[nodiscard]] constexpr int64_t nano_second() const;

    [[nodiscard]] local_time trunc_hour() const;
    [[nodiscard]] local_time trunc_minute() const;
    [[nodiscard]] local_time trunc_second() const;
    [[nodiscard]] local_time trunc_nano_second() const;

    constexpr bool is_noon() const;
    constexpr bool is_midnight() const;

    long double constexpr to_hours() const;
    long double constexpr to_minutes() const;
    long double constexpr to_seconds() const;
    int64_t constexpr to_nano_seconds() const;

    local_time operator+(const local_time & rhs) const;
    local_time operator-(const local_time & rhs) const;
    bool operator>(const local_time & rhs) const;
    bool operator<(const local_time & rhs) const;
    bool operator>=(const local_time & rhs) const;
    bool operator<=(const local_time & rhs) const;
    bool operator==(const local_time & rhs) const;
    bool operator!=(const local_time & rhs) const;

    friend std::ostream& operator<<(std::ostream& stream, const local_time & obj);
};

class local_time_formatter {

public:
    static std::string format(
            char separator,
            int64_t hour,
            int64_t minute,
            int64_t second,
            int64_t nano_second);

};

class local_time_validator {
public:
    static void validate(
            int64_t hour,
            int64_t minute,
            int64_t second,
            int64_t nano_second);

private:
    static void validate_hour(int64_t hour);
    static void validate_minute(int64_t minute);
    static void validate_second(int64_t second);
    static void validate_nano_second(int64_t nano_second);

};

std::string local_time_formatter::format(
        char separator,
        int64_t hour,
        int64_t minute,
        int64_t second,
        int64_t nano_second) {

  return std::to_string(hour)
         + separator
         + std::to_string(minute)
         + separator
         + std::to_string(second)
         + separator
         + std::to_string(nano_second);
}

void local_time_validator::validate_hour(int64_t hour) {
  if (hour < 0 || hour > 23) {
    throw std::out_of_range("hour must be between (0, 23) inclusive, actual : " + std::to_string(hour));
  }
}

void local_time_validator::validate_minute(int64_t minute) {
  if (minute < 0 || minute > 59) {
    throw std::out_of_range("minute must be between (0, 59) inclusive, actual : " + std::to_string(minute));
  }
}

void local_time_validator::validate_second(int64_t second) {
  if (second < 0 || second > 59) {
    throw std::out_of_range("second must be between (0, 59) inclusive, actual : " + std::to_string(second));
  }
}

void local_time_validator::validate_nano_second(int64_t nano_second) {
  if (nano_second < 0 || nano_second > 999999999) {
    throw std::out_of_range("nano_second must be between (0, 999999999) inclusive, actual : " + std::to_string(nano_second));
  }
}

void local_time_validator::validate(
        int64_t hour,
        int64_t minute,
        int64_t second,
        int64_t nano_second) {
  validate_hour(hour);
  validate_minute(minute);
  validate_second(second);
  validate_nano_second(nano_second);
}


local_time::local_time(int64_t hour, int64_t minute, int64_t second, int64_t nanosecond)
        : hour_(hour), minute_(minute), second_(second), nano_second_(nanosecond),
          format_(local_time_formatter::format(':', hour, minute, second, nanosecond)) {
  local_time_validator::validate(hour, minute, second, nanosecond);
}

[[nodiscard]] constexpr int64_t local_time::hour() const {
  return hour_;
}

[[nodiscard]] constexpr int64_t local_time::minute() const {
  return minute_;
}

[[nodiscard]] constexpr int64_t local_time::second() const {
  return second_;
}

[[nodiscard]] constexpr int64_t local_time::nano_second() const {
  return nano_second_;
}

long double constexpr local_time::to_hours() const {
  return to_nano_seconds() / (60 * 60 * 1000000000.0);
}

long double constexpr local_time::to_minutes() const {
  return to_nano_seconds() / (60 * 1000000000.0);
}

long double constexpr local_time::to_seconds() const {
  return to_nano_seconds() / 1000000000.0;
}

int64_t constexpr local_time::to_nano_seconds() const {
  return ((int64_t)((hour_ * 60 * 60) + (minute_ * 60) + second_) * 1000000000) + nano_second_;
}

[[nodiscard]] local_time local_time::plus_hour(int64_t hour) const {
  return local_time_with_wrap(hour_ + hour,
                              minute_,
                              second_,
                              nano_second_);
}

[[nodiscard]] local_time local_time::plus_minute(int64_t minute) const {
  return local_time_with_wrap(hour_,
                              minute_ + minute,
                              second_,
                              nano_second_);
}

[[nodiscard]] local_time local_time::plus_second(int64_t second) const {
  return local_time_with_wrap(hour_,
                              minute_,
                              second_ + second,
                              nano_second_);
}

[[nodiscard]] local_time local_time::plus_nano_second(int64_t nano_second) const {
  return local_time_with_wrap(hour_,
                              minute_,
                              second_,
                              nano_second_ + nano_second);
}

bool local_time::operator>(const local_time & rhs) const {
  return to_nano_seconds() > rhs.to_nano_seconds();
}

bool local_time::operator<(const local_time & rhs) const {
  return to_nano_seconds() < rhs.to_nano_seconds();
}

bool local_time::operator>=(const local_time & rhs) const {
  return to_nano_seconds() >= rhs.to_nano_seconds();
}

bool local_time::operator<=(const local_time & rhs) const {
  return to_nano_seconds() <= rhs.to_nano_seconds();
}

bool local_time::operator==(const local_time & rhs) const {
  return to_nano_seconds() == rhs.to_nano_seconds();
}

bool local_time::operator!=(const local_time & rhs) const {
  return to_nano_seconds() != rhs.to_nano_seconds();
}

local_time local_time::operator+(const local_time & rhs) const {
  return local_time_with_wrap(
          hour_ + rhs.hour(),
          minute_ + rhs.minute(),
          second_ + rhs.minute(),
          nano_second_ + rhs.nano_second());
}

local_time local_time::operator-(const local_time &rhs) const {
  return local_time_with_wrap(
          hour_ - rhs.hour(),
          minute_ - rhs.minute(),
          second_ - rhs.second(),
          nano_second_ - rhs.nano_second());
}

std::ostream& operator<<(std::ostream& stream, const local_time & obj) {
  stream << obj.format_;
  return stream;
}

[[nodiscard]] local_time local_time::trunc_hour() const {
  return {0, minute_, second_, nano_second_};
}

[[nodiscard]] local_time local_time::trunc_minute() const {
  return {hour_, 0, second_, nano_second_};
}

[[nodiscard]] local_time local_time::trunc_second() const {
  return {hour_, minute_, 0, nano_second_};
}

[[nodiscard]] local_time local_time::trunc_nano_second() const {
  return {hour_, minute_, second_, 0};
}

constexpr const std::string& local_time::to_string() const {
  return format_;
}

[[nodiscard]] local_time local_time::minus_hour(int64_t hour) const {
  return {hour_ - hour,
          minute_,
          second_,
          nano_second_};
}

[[nodiscard]] local_time local_time::minus_minute(int64_t minute) const {
  return {hour_,
          minute_ - minute,
          second_,
          nano_second_};
}

[[nodiscard]] local_time local_time::minus_second(int64_t second) const {
  return {hour_,
          minute_,
          second_ - second,
          nano_second_};
}

[[nodiscard]] local_time local_time::minus_nano_second(int64_t nano_second) const {
  return {hour_,
          minute_,
          second_,
          nano_second_ - nano_second};
}

constexpr bool local_time::is_noon() const {
  return hour_ == 12 && minute_ == 0 && second_ == 0 && nano_second_ == 0;
}

constexpr bool local_time::is_midnight() const {
  return hour_ == 0 && minute_ == 0 && second_ == 0 && nano_second_ == 0;
}

[[nodiscard]] local_time local_time::of(int64_t hour) {
  return {hour, 0, 0, 0};
}

[[nodiscard]] local_time local_time::of(int64_t hour, int64_t minute) {
  return {hour, minute, 0, 0};
}

[[nodiscard]] local_time local_time::of(int64_t hour, int64_t minute, int64_t second) {
  return {hour, minute, second, 0};
}

[[nodiscard]] local_time local_time::of(int64_t hour, int64_t minute, int64_t second, int64_t nano_second) {
  return {hour, minute, second, nano_second};
}

[[nodiscard]] local_time local_time::now() {
  int64_t now = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  return local_time_with_wrap(0, 0, 0, now);
}

local_time local_time::local_time_with_wrap(int64_t hour, int64_t minute, int64_t second, int64_t nanosecond) {
  auto rem_nano_seconds = nanosecond % 1000000000L;
  auto rem_seconds = second + (nanosecond / 1000000000L);
  auto rem_minutes = minute + rem_seconds / 60L;
  auto rem_hours = hour + rem_minutes / 60L;
  rem_seconds = (rem_seconds % 60) + 60;
  rem_minutes = (rem_minutes % 60) + 60;
  rem_hours = (rem_hours % 24) + 24;
  rem_nano_seconds = rem_nano_seconds + 1000000000L;
  return {rem_hours % 24 , rem_minutes % 60, rem_seconds % 60, rem_nano_seconds % 1000000000L};
}

#endif //LOCAL_TIME_V1_LOCAL_TIME_H
